package com.rover12421.crack.jerbrains.v2;

import com.rover12421.asm.ClassReader;
import com.rover12421.asm.ClassWriter;
import com.rover12421.asm.Opcodes;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;
import java.util.List;

/**
 * Created by rover12421 on 11/9/15.
 */
public class Agent {
    public static void premain(String args, Instrumentation inst) {
        System.out.println("*********************************");
        System.out.println("*      Jerbrains v2.1 Crack     *");
        System.out.println("*    By : Rover12421@163.com    *");
        System.out.println("*   Http://Www.Rover12421.Com   *");
        System.out.println("*********************************");
        inst.addTransformer(new MethodEntryTransformer());
    }

    static class MethodEntryTransformer implements ClassFileTransformer {

        public byte[] transform(ClassLoader loader, String className,
                                Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer)
                throws IllegalClassFormatException {
            try {
                if (className.equals("com/jetbrains/ls/newLicenses/DecodeCertificates")) {
                    System.out.println("Find class DecodeCertificates !");
                    ClassReader cr = new ClassReader(classfileBuffer);
                    com.rover12421.asm.tree.ClassNode cn = new com.rover12421.asm.tree.ClassNode();
                    cr.accept(cn, 0);
                    List<com.rover12421.asm.tree.MethodNode> methodNodes = cn.methods;
                    for (com.rover12421.asm.tree.MethodNode methodNode : methodNodes) {
                        if ("decodeLicense".equals(methodNode.name)) {
                            System.out.println("Find Method decodeLicense");
                            com.rover12421.asm.tree.InsnList insns = methodNode.instructions;
                            insns.clear();
                            insns.add(new com.rover12421.asm.tree.VarInsnNode(Opcodes.ALOAD, 1));
                            insns.add(new com.rover12421.asm.tree.InsnNode(Opcodes.ARETURN));
                            methodNode.visitEnd();
                            ClassWriter cw = new ClassWriter(0);
                            cn.accept(cw);
                            return cw.toByteArray();
                        }
                    }
                }

            } catch (Exception e) {
                return classfileBuffer;
            }

            return classfileBuffer;
        }
    }

}
